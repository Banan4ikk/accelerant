import React from 'react';
import Routes from './view/routes';

function App() {
  return <Routes />;
}

export default App;
