export const API_URL = 'https://accelerist.herokuapp.com/api/v1';
export const SIGN_UP_URL = 'auth/sign_up';
export const SIGN_IN_URL = 'auth/sign_in';
export const emailPattern = '^(.+)@(.+)$';
