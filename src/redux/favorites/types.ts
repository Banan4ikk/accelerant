import {companyInfo, meta} from '../types';

export type favoritesResponse = {
  items: companyInfo[];
  meta: meta;
};
