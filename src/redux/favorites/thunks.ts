import {createAsyncThunk} from '@reduxjs/toolkit';
import {http} from '../../services/http';
import {AxiosResponse} from 'axios';

export const getFavorites = createAsyncThunk(
  'companies/favorites',
  async () => {
    const {data}: AxiosResponse = await http.get(
      `companies/favorites?page=1&limit=100`,
    );
    return data;
  },
);
