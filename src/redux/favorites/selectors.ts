import {rootState} from '../store';
import {createSelector} from '@reduxjs/toolkit';

export const selectFavorites = (state: rootState) => state.favorites.items;

export const selectCompanyData = (companyId: string) =>
  createSelector(selectFavorites, state => {
    return state.filter(item => item.id === companyId)[0];
  });
