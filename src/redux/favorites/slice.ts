import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {favoritesResponse} from './types';
import {getFavorites} from './thunks';

const initialState: favoritesResponse = {} as favoritesResponse;

const CorporateSlice = createSlice({
  initialState,
  name: 'favorites',
  reducers: {},
  extraReducers: builder => {
    builder.addCase(
      getFavorites.fulfilled,
      (state, {payload}: PayloadAction<favoritesResponse>) => {
        return payload;
      },
    );
    builder.addCase(getFavorites.rejected, state => {
      return state;
    });
  },
});

const reducer = CorporateSlice.reducer;
const actions = {...CorporateSlice.actions};

export {reducer, actions};
