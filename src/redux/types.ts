import {User} from './auth/types';

export type meta = {
  totalItems: number;
  itemCount: number;
  itemsPerPage: string;
  totalPages: number;
  currentPage: string;
};

export type companyInfo = {
  id: string;
  zoomInfoId: string | null;
  name: string;
  logo: null | string;
  ticker: string;
  parentCompany: string | null;
  phone: string;
  fax: string;
  website: string;
  city: string;
  street: string;
  state: string;
  zipCode: number;
  country: string;
  continent: string | null;
  productsBrandDescription: string | null;
  descriptionList: string;
  revenueRange: string;
  employeeRange: number;
  twitterHandle: string | null;
  socialMediaUrls: string | null;
  competitors: string | null;
  subUnitIndustries: string | null;
  primaryIndustry: string[];
  industries: string | null;
  revenue: number;
  employeeCount: number;
  annualContributions: string | null;
  cashContributions: string | null;
  inKindContributions: string | null;
  employeeContributions: string | null;
  parentId: string | null;
  parentName: string | null;
  type: string | null;
  sdgGoals: [];
  genders: string | null;
  income: string | null;
  age: string | null;
  ethnicity: string | null;
  nonprofit: string | null;
  purchase: string | null;
  affiliation: string | null;
  brands: string | null;
  interests: string | null;
  typesOfInvestment: string | null;
  errorLoadZoomInfo: string | null;
  charitablePartners: [];
  statusZoomInfo: string;
  loadZoomInfoDate: string | null;
  errorLoadZoomInfoDate: string | null;
  partnershipLink: string | null;
  employeeEngagementOpportunities: true;
  similarCompanies: string;
  favoriteCompanies: favorites[];
  like: boolean | undefined;
  score: number;
  csrFocus: [];
};

type favorites = {
  id: string;
  companyId: string;
  userId: string;
  company: companyInfo[];
  user: userCorporate;
};

interface userCorporate extends User {
  loggedInAt: string;
  deletedAt: string;
}
