import {createAsyncThunk} from '@reduxjs/toolkit';
import {http} from '../../services/http';
import {AxiosResponse} from 'axios';
import {filterItems} from './types';

export const getCompanies = createAsyncThunk(
  '/companies',
  async (pageNumber: number) => {
    const {data}: AxiosResponse = await http.get(
      `companies?page=${pageNumber}&limit=12`,
    );
    return data;
  },
);

export const likeCompany = createAsyncThunk(
  'companies/id/like',
  async (companyId: string) => {
    const {data}: AxiosResponse = await http.get(`companies/${companyId}/like`);
    return {data, companyId};
  },
);

export const dislikeCompany = createAsyncThunk(
  'companies/id/dislike',
  async (companyId: string) => {
    await http.get(`companies/${companyId}/dislike`);
    return {companyId};
  },
);

export const filerCompanies = createAsyncThunk(
  'companies/filer',
  async (values: filterItems) => {
    const {householdIncome} = values;
    const {data}: AxiosResponse = await http.get(
      `companies?page=1&limit=12&income=${householdIncome}`,
    );
    return data;
  },
);
