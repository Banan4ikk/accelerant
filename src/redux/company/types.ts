import {companyInfo, meta} from '../types';

export type companiesResponse = {
  items: companyInfo[];
  meta: meta;
};

export type filterItems = {
  // ageRanging: string;
  householdIncome: string;
};
