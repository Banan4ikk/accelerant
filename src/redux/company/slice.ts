import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {
  dislikeCompany,
  filerCompanies,
  getCompanies,
  likeCompany,
} from './thunks';
import {companiesResponse} from './types';

const initialState: companiesResponse = {
  items: [],
  meta: {
    totalItems: 0,
    totalPages: 0,
    itemCount: 12,
    itemsPerPage: '1',
    currentPage: '1',
  },
};

const CorporateSlice = createSlice({
  initialState,
  name: 'company',
  reducers: {},
  extraReducers: builder => {
    builder.addCase(
      getCompanies.fulfilled,
      (state, {payload}: PayloadAction<companiesResponse>) => {
        return payload;
      },
    );
    builder.addCase(getCompanies.rejected, () => {
      return initialState;
    });

    builder.addCase(
      likeCompany.fulfilled,
      (state, {payload}: PayloadAction<{data: boolean; companyId: string}>) => {
        const {data, companyId} = payload;
        const company = state.items.find(item => item.id === companyId);

        if (company) {
          company.like = data;
        }

        return state;
      },
    );
    builder.addCase(likeCompany.rejected, state => {
      return state;
    });

    builder.addCase(
      dislikeCompany.fulfilled,
      (state, {payload}: PayloadAction<{companyId: string}>) => {
        const {companyId} = payload;
        const company = state.items.find(item => item.id === companyId);

        if (company) {
          company.like = false;
        }

        return state;
      },
    );
    builder.addCase(dislikeCompany.rejected, state => {
      return state;
    });

    builder.addCase(
      filerCompanies.fulfilled,
      (state, {payload}: PayloadAction<companiesResponse>) => {
        return payload;
      },
    );
    builder.addCase(filerCompanies.rejected, state => state);
  },
});

const reducer = CorporateSlice.reducer;
const actions = {...CorporateSlice.actions};

export {reducer, actions};
