import {rootState} from '../store';
import {createSelector} from '@reduxjs/toolkit';

export const selectCompanies = (state: rootState) => state.corporate.items;
export const selectMeta = (state: rootState) => state.corporate.meta;

export const selectCompanyData = (companyId: string) =>
  createSelector(selectCompanies, state => {
    return state.filter(item => item.id === companyId)[0];
  });
