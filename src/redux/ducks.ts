import {combineReducers} from 'redux';
import * as auth from './auth';
import * as corporate from './company';
import * as favorites from './favorites';
import * as companyData from './companyData';

export const rootReducer = combineReducers({
  auth: auth.reducer,
  corporate: corporate.reducer,
  favorites: favorites.reducer,
  companyData: companyData.reducer,
});

export const actions = {
  auth: auth.actions,
  corporate: corporate.actions,
  favorites: favorites.actions,
  companyData: companyData.actions,
};

export const selectors = {
  auth: auth.selectors,
  corporate: corporate.selectors,
  favorites: favorites.selectors,
  companyData: companyData.selectors,
};
