import {companyInfo} from '../types';
import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {getCompanyData} from './thunks';

const initialState: companyInfo = {} as companyInfo;

const CompanySlice = createSlice({
  name: 'company',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(
      getCompanyData.fulfilled,
      (state, {payload}: PayloadAction<companyInfo>) => payload,
    );
    builder.addCase(getCompanyData.rejected, state => state);
  },
});

const reducer = CompanySlice.reducer;
const actions = {...CompanySlice.actions};

export {reducer, actions};
