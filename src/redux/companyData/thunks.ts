import {createAsyncThunk} from '@reduxjs/toolkit';
import {AxiosResponse} from 'axios';
import {http} from '../../services/http';

export const getCompanyData = createAsyncThunk(
  'companies/id',
  async (companyId: string) => {
    const {data}: AxiosResponse = await http.get(`companies/${companyId}`);
    return data;
  },
);
