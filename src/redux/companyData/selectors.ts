import {rootState} from '../store';

export const selectData = (state: rootState) => {
  return state.companyData;
};
