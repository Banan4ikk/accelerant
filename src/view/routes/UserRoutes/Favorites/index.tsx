import React, {useEffect} from 'react';
import AppSubHeader from '../../../ui/AppSubHeader';
import {StyledContainerStart} from '../generalStyles';
import {useDispatch, useSelector} from 'react-redux';
import {selectors} from '../../../../redux/ducks';
import SearchCard from '../../../components/SearchCard';
import {getFavorites} from '../../../../redux/favorites/thunks';
import EmptyFavorites from '../../../ui/EmptyFavorites';

const Favorites = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getFavorites());
  }, []);

  const favorites = useSelector(selectors.favorites.selectFavorites);

  return (
    <StyledContainerStart marginTop={140} height="65vh">
      <AppSubHeader title="Favorites" />
      {favorites.length === 0 ? (
        <EmptyFavorites width="100%" height="100%" />
      ) : (
        favorites.map(item => (
          <SearchCard key={item.id} companyId={item.id} favorite={true} />
        ))
      )}
    </StyledContainerStart>
  );
};

export default Favorites;
