import styled from 'styled-components';

export const StyledContainer = styled.div`
  width: 100vw;
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
  margin-top: 140px;
`;
