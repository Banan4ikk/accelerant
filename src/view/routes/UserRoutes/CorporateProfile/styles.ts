import styled from 'styled-components';

export const CorporateContainer = styled.div`
  margin-top: 130px;
  width: fit-content;
  min-height: 100vh;
  height: fit-content;
  display: flex;
  flex-direction: column;
`;

export const HeaderInfoContainer = styled.div`
  padding: 40px;
  background: #f2f2f2;
  width: 80vw;
  border-radius: 6px;
  display: flex;
  justify-content: flex-start;
  align-items: center;
  margin-right: 25px;
`;

export const StyledLogo = styled.img`
  margin-right: 25px;
`;

export const TextContainer = styled.div`
  height: 100%;
  width: fit-content;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

export const Title = styled.div`
  font-weight: 500;
  font-size: 24px;
  color: #122434;
  margin: 5px 0;
`;

export const GlobalContainer = styled.div`
  width: 80vw;
  background: #fff;
  height: fit-content;
`;

export const CompanyContentContainer = styled.div`
  width: 55vw;
  background: #fff;
  min-height: 100vh;
  display: flex;
  border-right: 1px #ebebeb solid;
`;

export const CompanyInfoContainer = styled.div`
  width: 100%;
  height: fit-content;
  padding: 40px;
`;

export const ReportedContainer = styled.div`
  display: flex;
  height: fit-content;
  transform: translateY(-30px);
  margin-bottom: 15px;
`;

export const TickerContainer = styled.div`
  display: flex;
`;

export const ContactsContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

export const FlexContainer = styled.div`
  width: fit-content;
  display: flex;
  margin-bottom: 10px;
`;
