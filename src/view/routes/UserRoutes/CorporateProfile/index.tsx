import React, {useEffect} from 'react';
import {
  CompanyContentContainer,
  CompanyInfoContainer,
  ContactsContainer,
  FlexContainer,
  GlobalContainer,
  HeaderInfoContainer,
  ReportedContainer,
  StyledLogo,
  TextContainer,
  TickerContainer,
  Title,
} from './styles';
import AppSubHeader from '../../../ui/AppSubHeader';
import {
  StyledCardTitle,
  StyledContainerStart,
  StyledTextBig,
  StyledTextGray,
  StyledTitle,
} from '../generalStyles';
import NumbersItem from '../../../ui/NumbersItem';
import BorderedCard from '../../../components/BorderedCard';
import SearchHeaderActions from '../../../ui/SearchHeaderActions';
import {useLocation} from 'react-router-dom';
import {useDispatch, useSelector} from 'react-redux';
import {getCompanyData} from '../../../../redux/companyData/thunks';
import {selectors} from '../../../../redux/ducks';

interface stateType {
  id: string;
}

const CorporateProfile = () => {
  const {state} = useLocation();
  const {id} = state as stateType;
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getCompanyData(id));
  }, []);

  const data = useSelector(selectors.companyData.selectData);

  const {
    name,
    descriptionList,
    ticker,
    revenue,
    employeeCount,
    country,
    state: countyState,
    city,
    street,
    phone,
    website,
    logo,
    zipCode,
    productsBrandDescription,
  } = data;

  return (
    <StyledContainerStart marginTop={140}>
      <AppSubHeader title="Corporate Profile" isBack />
      <HeaderInfoContainer>
        <StyledLogo src={logo ? logo : '/images/empty-logo.svg'} />
        <TextContainer>
          <Title>{name}</Title>
          <StyledTextGray>
            {productsBrandDescription
              ? productsBrandDescription
              : 'No description'}
          </StyledTextGray>
        </TextContainer>
      </HeaderInfoContainer>
      <GlobalContainer>
        <CompanyContentContainer>
          <CompanyInfoContainer>
            <StyledTitle>Business Description Products</StyledTitle>
            <StyledCardTitle>Description</StyledCardTitle>
            <StyledTextBig>{descriptionList}</StyledTextBig>
            <StyledCardTitle>Reported</StyledCardTitle>
            <ReportedContainer>
              <NumbersItem
                title="Revenue Reported"
                number={revenue}
                background="#fff"
                border="1px #e8e8e8 solid"
              />
              <NumbersItem
                title="Employees Reported"
                number={employeeCount}
                background="#fff"
                border="1px #e8e8e8 solid"
              />
            </ReportedContainer>
            <StyledCardTitle>Company Ticker</StyledCardTitle>
            <TickerContainer>
              <BorderedCard width="fit-content">
                <Title>{ticker ? ticker : 'No information'}</Title>
              </BorderedCard>
            </TickerContainer>
            <StyledCardTitle>Company Contacts</StyledCardTitle>
            <TickerContainer>
              <BorderedCard width="100%">
                <ContactsContainer>
                  <FlexContainer>
                    <SearchHeaderActions
                      imgPath="/images/globe.svg"
                      title={website}
                    />
                    <SearchHeaderActions
                      imgPath="/images/phone.svg"
                      title={phone}
                    />
                  </FlexContainer>
                  <SearchHeaderActions
                    imgPath="/images/map-pin.svg"
                    title={`${street} ${city} ${countyState} ${country} ${
                      zipCode ?? ''
                    }`}
                  />
                </ContactsContainer>
              </BorderedCard>
            </TickerContainer>
            <StyledTitle>Social Impact</StyledTitle>
            <BorderedCard width="fit-content">
              <Title>No information</Title>
            </BorderedCard>
          </CompanyInfoContainer>
        </CompanyContentContainer>
      </GlobalContainer>
    </StyledContainerStart>
  );
};

export default CorporateProfile;
