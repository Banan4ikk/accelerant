import React, {useEffect} from 'react';
import AppWrapper from '../../components/AppWrapper';
import {Route, Routes, useNavigate} from 'react-router-dom';
import Dashboard from './Dashboard';
import CorporateProfile from './CorporateProfile';
import Prospects from './Prospects';
import AccountingServices from './AccoutingService';
import Favorites from './Favorites';
import {
  DASHBOARD_ROUTE,
  FAVORITES_ROUTE,
  PROFILE_ROUTE,
  PROSPECTS_ROUTE,
  SEARCH_ROUTE,
  SERVICES_ROUTE,
} from './routes';
import SearchScreen from './SeaechScreen';

const UserScreen = () => {
  const navigate = useNavigate();

  useEffect(() => {
    navigate(DASHBOARD_ROUTE);
  }, []);

  return (
    <AppWrapper>
      <Routes>
        <Route path={DASHBOARD_ROUTE} element={<Dashboard />} />
        <Route path={SEARCH_ROUTE} element={<SearchScreen />} />
        <Route path={PROFILE_ROUTE} element={<CorporateProfile />} />
        <Route path={PROSPECTS_ROUTE} element={<Prospects />} />
        <Route path={SERVICES_ROUTE} element={<AccountingServices />} />
        <Route path={FAVORITES_ROUTE} element={<Favorites />} />
      </Routes>
    </AppWrapper>
  );
};

export default UserScreen;
