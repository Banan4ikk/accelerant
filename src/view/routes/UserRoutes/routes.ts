export const SERVICES_ROUTE = '/services';
export const DASHBOARD_ROUTE = '/dashboard';
export const PROSPECTS_ROUTE = '/prospects';
export const SEARCH_ROUTE = '/search';
export const PROFILE_ROUTE = '/profile';
export const FAVORITES_ROUTE = '/favorites';
