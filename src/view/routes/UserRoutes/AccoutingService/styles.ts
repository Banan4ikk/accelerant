import styled from 'styled-components';

export const TitleContainer = styled.div`
  width: 100%;
  justify-content: flex-start;
  margin-bottom: 20px;
`;

export const FiltersContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const FiltersItemsContainer = styled.div`
  display: flex;
  width: fit-content;
  height: fit-content;
  margin-bottom: 20px;
`;

export const CardsContainer = styled.div`
  display: flex;
  justify-content: flex-start;
`;
