import React from 'react';
import {
  StyledCardTitle,
  StyledContainerCenter,
  StyledContainerStart,
  StyledTextGray,
} from '../generalStyles';
import AppSubHeader from '../../../ui/AppSubHeader';
import EditHeaderButtons from '../../../pageComponents/AccountingServicesPage/EditHeaderButtons';
import {
  FiltersContainer,
  FiltersItemsContainer,
  TitleContainer,
} from './styles';
import DashboardFilterItem from '../../../ui/DashboardFilterItem';
import SearchHeaderActions from '../../../ui/SearchHeaderActions';

const AccountingServices = () => {
  return (
    <StyledContainerCenter>
      <AppSubHeader title="Accounting Services" spaceBetween>
        <EditHeaderButtons />
      </AppSubHeader>
      <TitleContainer>
        <StyledCardTitle>5 companies</StyledCardTitle>
        <FiltersContainer>
          <StyledTextGray>Filters</StyledTextGray>
        </FiltersContainer>
        <FiltersItemsContainer>
          <DashboardFilterItem title="Gender: Both" />
          <DashboardFilterItem title="Accounting Services" />
        </FiltersItemsContainer>
        <SearchHeaderActions
          imgPath="/images/upload.svg"
          title="Export to Excel"
        />
      </TitleContainer>
      <StyledContainerStart marginTop={0}>
        {/*<SearchCard />*/}
      </StyledContainerStart>
    </StyledContainerCenter>
  );
};

export default AccountingServices;
