import React, {useEffect} from 'react';
import {
  CardsContainer,
  CompaniesTitleContainer,
  FavoriteContainer,
  FlexContainer,
  InsideCardTitileContainer,
  LoginItem,
  LoginName,
  LoginNameContainer,
  MatchedLogoContainer,
  MatchedLogoItem,
  NumbersItemContainer,
  ReportsHeader,
  StyledCardTitle,
  StyledDate,
  StyledTitle,
  TitleContainer,
} from './styles';
import DashboardCard from '../../../ui/DashboardCard';
import NumbersItem from '../../../ui/NumbersItem';
import FavoriteCard from '../../../components/FavoriteCard';
import AppSubHeader from '../../../ui/AppSubHeader';
import {StyledContainerStart, StyledLink} from '../generalStyles';
import DashboardCompanyCard from '../../../components/DahboardCompamyCard';
import {FAVORITES_ROUTE} from '../routes';
import {useDispatch, useSelector} from 'react-redux';
import {getFavorites} from '../../../../redux/favorites/thunks';
import {selectors} from '../../../../redux/ducks';
import EmptyFavorites from '../../../ui/EmptyFavorites';

const Dashboard = () => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getFavorites());
  }, []);

  const favorites = useSelector(selectors.favorites.selectFavorites);

  return (
    <StyledContainerStart marginTop={140}>
      <AppSubHeader title="Dashboard" />
      <StyledTitle>Prospecting Sessions</StyledTitle>
      <CardsContainer>
        <DashboardCompanyCard />
        <DashboardCompanyCard />
      </CardsContainer>
      <CompaniesTitleContainer>
        <TitleContainer>
          <StyledLink to={FAVORITES_ROUTE}>
            <StyledTitle>Favorite</StyledTitle>
          </StyledLink>
        </TitleContainer>
        <TitleContainer>
          <StyledTitle>Reports</StyledTitle>
        </TitleContainer>
      </CompaniesTitleContainer>
      <CardsContainer>
        <FavoriteContainer
          height={
            favorites && favorites.length === 0 ? undefined : 'fit-content'
          }>
          {favorites && favorites.length === 0 ? (
            <EmptyFavorites width="100%" height="100%" />
          ) : (
            favorites &&
            favorites.map(item => <FavoriteCard key={item.id} id={item.id} />)
          )}
        </FavoriteContainer>
        <DashboardCard>
          <ReportsHeader>
            <FlexContainer>
              <TitleContainer>
                <StyledCardTitle>Search Sessions</StyledCardTitle>
              </TitleContainer>
              <TitleContainer>
                <StyledCardTitle>Sent Pitches</StyledCardTitle>
              </TitleContainer>
            </FlexContainer>
            <NumbersItemContainer>
              <NumbersItem title="Total" number={43} background="#f9f9f9" />
              <NumbersItem title="Company" number={44} background="#f9f9f9" />
            </NumbersItemContainer>
          </ReportsHeader>
          <InsideCardTitileContainer>
            <StyledCardTitle>Top Matched</StyledCardTitle>
          </InsideCardTitileContainer>
          <MatchedLogoContainer>
            <MatchedLogoItem>
              <img src="/images/samsung.png" />
            </MatchedLogoItem>
            <MatchedLogoItem>
              <img src="/images/samsung.png" />
            </MatchedLogoItem>
            <MatchedLogoItem>
              <img src="/images/samsung.png" />
            </MatchedLogoItem>
            <MatchedLogoItem>
              <img src="/images/samsung.png" />
            </MatchedLogoItem>
          </MatchedLogoContainer>
          <InsideCardTitileContainer>
            <StyledCardTitle>Last Login</StyledCardTitle>
          </InsideCardTitileContainer>
          <LoginItem>
            <img src="/images/ava2.png" />
            <LoginNameContainer>
              <LoginName>Frank Lampard</LoginName>
              <StyledDate>12 Aug 2020</StyledDate>
            </LoginNameContainer>
          </LoginItem>
          <LoginItem>
            <img src="/images/ava2.png" />
            <LoginNameContainer>
              <LoginName>Frank Lampard</LoginName>
              <StyledDate>12 Aug 2020</StyledDate>
            </LoginNameContainer>
          </LoginItem>
        </DashboardCard>
      </CardsContainer>
    </StyledContainerStart>
  );
};

export default Dashboard;
