import styled from 'styled-components';

type Props = {
  height?: string;
};

export const DashboardContainer = styled.div`
  height: fit-content;
  width: 100%;
  margin-top: 15vh;
  padding: 30px 0;
`;

export const StyledTitle = styled.div`
  font-weight: 500;
  font-size: 24px;
  color: #122434;
  margin: 30px 0;
`;

export const StyledCardTitle = styled.h2`
  font-weight: 500;
  font-size: 16px;
`;

export const NumberCardContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const CardsContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  height: fit-content;
  width: 100%;
`;

export const StyledName = styled.p`
  font-size: 12px;
  font-weight: 500;
  color: #122434;
`;

export const StyledTitleActivity = styled.p`
  font-size: 12px;
  font-weight: 400;
  color: #737373;
`;

export const FavoriteContainer = styled.div<Props>`
  width: 45%;
  height: ${props => props.height};
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  margin-right: 25px;
`;

export const CompaniesTitleContainer = styled.div`
  display: flex;
  width: 90%;
  justify-content: space-between;
`;

export const FlexContainer = styled.div`
  display: flex;
`;

export const TitleContainer = styled.div`
  width: 45%;
  margin-right: 25px;
`;

export const ReportsHeader = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
`;

export const NumbersItemContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const InsideCardTitileContainer = styled.div`
  margin: 20px 0;
`;

export const MatchedLogoContainer = styled.div`
  display: flex;
`;

export const MatchedLogoItem = styled.div`
  margin-right: 10px;
  &:last-child {
    margin-right: 0;
  }
`;

export const LoginItem = styled.div`
  width: 100%;
  height: fit-content;
  display: flex;
  margin-top: 20px;
  &:first-child {
    margin-top: 0;
  }
`;

export const LoginNameContainer = styled.div`
  margin-left: 10px;
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  &::after {
    content: '';
    height: 1px;
    background: #eeeeee;
    position: absolute;
    width: 34%;
    margin-top: 25px;
  }
`;

export const LoginName = styled.p`
  font-size: 12px;
  font-weight: 500;
  color: #122434;
`;

export const StyledDate = styled.p`
  font-size: 12px;
  font-weight: 400;
  color: #737373;
`;
