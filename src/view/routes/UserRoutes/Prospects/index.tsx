import React from 'react';
import AppSubHeader from '../../../ui/AppSubHeader';
import {StyledContainerCenter, StyledText} from '../generalStyles';
import {MenuContainer, MenuItem, MenuItemsContainer} from './styles';
import DashboardCompanyCard from '../../../components/DahboardCompamyCard';

const Prospects = () => {
  return (
    <StyledContainerCenter>
      <AppSubHeader title="Prospects" />
      <MenuContainer>
        <MenuItemsContainer>
          <StyledText>Sort By</StyledText>
          <MenuItem>
            <StyledText>Alphabet</StyledText>
          </MenuItem>
          <MenuItem>
            <StyledText>Prospects Available</StyledText>
          </MenuItem>
          <MenuItem>
            <StyledText>Last Activity</StyledText>
          </MenuItem>
        </MenuItemsContainer>
        <StyledText>1-2 of 2</StyledText>
      </MenuContainer>
      <DashboardCompanyCard />
      <DashboardCompanyCard />
    </StyledContainerCenter>
  );
};

export default Prospects;
