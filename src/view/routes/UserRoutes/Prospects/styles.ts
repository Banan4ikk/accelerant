import styled from 'styled-components';

export const StyledContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  margin-top: 140px;
`;

export const MenuContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  height: fit-content;
  margin-bottom: 30px;
`;

export const MenuItemsContainer = styled.div`
  display: flex;
  width: 350px;
  justify-content: space-between;
`;

export const MenuItem = styled.a`
  height: fit-content;

  &:hover {
    cursor: pointer;
  }
  &:after {
    content: '';
    margin-top: 8px;
    position: relative;
    width: 0;
    display: flex;
    height: 2px;
    background: #122434;
    transition: width 0.2s ease-in-out;
  }
  &:hover::after {
    width: 100%;
  }
`;
