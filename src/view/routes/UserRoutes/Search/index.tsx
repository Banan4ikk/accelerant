import React, {FC, useEffect, useState} from 'react';
import {
  ActionsContainer,
  ArrowsContainer,
  CardsContainer,
  ContentContainer,
  PagesContainer,
  SearchHeaderContainer,
  StyledCardTitle,
  StyledText,
  SubhederActionsContainer,
} from './styles';
import SearchHeaderActions from '../../../ui/SearchHeaderActions';
import SearchCard from '../../../components/SearchCard';
import {StyledContainerStart} from '../generalStyles';
import useWindowDimensions from '../../../../utils/useWindowDemensios';
import 'rc-slider/assets/index.css';
import {useDispatch, useSelector} from 'react-redux';
import {getCompanies} from '../../../../redux/company/thunks';
import {selectors} from '../../../../redux/ducks';

const Search: FC = () => {
  const dispatch = useDispatch();
  const [pageCount, setPageCount] = useState(1);

  useEffect(() => {
    dispatch(getCompanies(pageCount));
  }, []);

  useEffect(() => {
    dispatch(getCompanies(pageCount));
  }, [pageCount]);

  const {width} = useWindowDimensions();

  const companies = useSelector(selectors.corporate.selectCompanies);
  const meta = useSelector(selectors.corporate.selectMeta);
  const {totalItems, itemCount} = meta;
  const [leftItemsCount, setLeftItemsCount] = useState(1);
  const [rightItemsCount, setRightItemsCount] = useState(itemCount);

  const onClickNext = () => {
    setPageCount(prevState => prevState + 1);
    setLeftItemsCount(prevState => prevState + itemCount);
    setRightItemsCount(prevState => prevState + itemCount);
  };

  const onClickPrevious = () => {
    setPageCount(prevState => prevState - 1);
    setLeftItemsCount(prevState => prevState - itemCount);
    setRightItemsCount(prevState => prevState - itemCount);
  };

  return (
    <StyledContainerStart marginTop={140}>
      <ContentContainer>
        <SearchHeaderContainer>
          {width <= 375 ? (
            <StyledText>Found {totalItems} companies</StyledText>
          ) : (
            <StyledCardTitle>Found {totalItems} companies</StyledCardTitle>
          )}
          <SubhederActionsContainer>
            <ActionsContainer>
              <SearchHeaderActions
                imgPath="/images/folder-plus.svg"
                title="Save List"
              />
              <SearchHeaderActions
                imgPath="/images/upload.svg"
                title="Export to Excel"
              />
              <SearchHeaderActions
                imgPath="/images/mail.svg"
                title="Accelerist Support"
              />
            </ActionsContainer>
            {width > 375 && (
              <PagesContainer>
                <ArrowsContainer onClick={onClickPrevious}>
                  {pageCount === 1 ? null : <img src="/images/leftArrow.svg" />}
                </ArrowsContainer>
                <StyledText>
                  {leftItemsCount}-{rightItemsCount} of {totalItems}
                </StyledText>
                <ArrowsContainer onClick={onClickNext}>
                  <img src="/images/rightArrow.svg" />
                </ArrowsContainer>
              </PagesContainer>
            )}
          </SubhederActionsContainer>
        </SearchHeaderContainer>
        <CardsContainer>
          {companies &&
            companies.map(item => (
              <SearchCard companyId={item.id} key={item.id} />
            ))}
        </CardsContainer>
      </ContentContainer>
    </StyledContainerStart>
  );
};

export default Search;
