import styled from 'styled-components';

export const CardsContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  height: fit-content;
  flex-wrap: wrap;
  width: 100%;
`;

export const ContentContainer = styled.div`
  width: 100%;
  height: fit-content;
  min-height: 100vh;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const SearchHeaderContainer = styled.div`
  width: 80%;
  height: fit-content;
  margin-bottom: 30px;
`;

export const StyledCardTitle = styled.p`
  color: #122434;
  font-size: 16px;
  font-weight: 500;
`;

export const StyledText = styled.p`
  color: #122434;
  font-size: 12px;
  font-weight: 400;
`;
export const ActionsContainer = styled.div`
  display: flex;
  margin-top: 25px;
`;

export const SubhederActionsContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const PagesContainer = styled.div`
  width: fit-content;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const PriorityContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 10px;
  border-top: 1px #e8e8e8 solid;
`;

export const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const ArrowsContainer = styled.div`
  height: fit-content;
  width: fit-content;
  padding: 0 15px;

  &:hover {
    cursor: pointer;
  }
`;
