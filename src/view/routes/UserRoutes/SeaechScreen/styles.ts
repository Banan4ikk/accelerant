import styled from 'styled-components';

export const SearchContainer = styled.div`
  width: 60%;
  height: fit-content;
  display: flex;
  justify-content: flex-end;
  align-items: center;
`;

export const StyledSearchIcon = styled.img`
  position: absolute;
  right: 28%;
  height: -10%;
  &:hover {
    cursor: pointer;
  }
`;

export const StyledFiltersIcon = styled.img`
  position: absolute;
  right: 30%;
  height: -55%;
  &:hover {
    cursor: pointer;
  }
`;
