import React, {useState} from 'react';
import FiltersForm from '../../../components/FiltersForm';
import Search from '../Search';
import SearchForm from '../../../components/SearchForm';
import AppSubHeader from '../../../ui/AppSubHeader';
import {SearchContainer, StyledFiltersIcon, StyledSearchIcon} from './styles';

const SearchScreen = () => {
  const [isShowFilters, setIsShowFilters] = useState(false);
  const onClickFilters = () => {
    setIsShowFilters(!isShowFilters);
  };
  return (
    <>
      <AppSubHeader title="Search">
        <SearchContainer>
          <StyledSearchIcon src="/images/search.svg" alt="search" />
          <StyledFiltersIcon
            src="/images/sliders.svg"
            alt="filters"
            onClick={onClickFilters}
          />
          <SearchForm />
        </SearchContainer>
      </AppSubHeader>
      {isShowFilters ? (
        <FiltersForm setIsShowFilters={setIsShowFilters} />
      ) : (
        <Search />
      )}
    </>
  );
};

export default SearchScreen;
