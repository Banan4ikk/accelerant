import styled from 'styled-components';
import {Link} from 'react-router-dom';

type Props = {
  marginTop?: number;
  width?: string;
  height?: string;
};

export const StyledTextGray = styled.p`
  color: #737373;
  font-size: 12px;
  font-weight: 400;
  margin-bottom: 7px;
`;

export const StyledTitle = styled.div`
  font-weight: 500;
  font-size: 24px;
  color: #122434;
  margin: 30px 0;
`;

export const StyledCardTitle = styled.p`
  color: #122434;
  font-size: 16px;
  font-weight: 500;
  margin: 15px 0;
`;

export const StyledText = styled.p`
  color: #122434;
  font-size: 12px;
  font-weight: 400;
`;
export const StyledTextBold = styled.p`
  color: #122434;
  font-size: 12px;
  font-weight: 500;
`;

export const StyledTextBig = styled.p`
  color: #122434;
  font-size: 16px;
  font-weight: 300;
`;

export const StyledContainerCenter = styled.div<Props>`
  width: 100%;
  height: ${props => props.height};
  display: flex;
  justify-content: center;
  flex-wrap: wrap;
  margin-top: 140px;
`;

export const StyledContainerStart = styled.div<Props>`
  width: 100%;
  height: ${props => props.height && props.height};
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
  margin-top: ${props => `${props.marginTop}px`};
`;

export const StyledLink = styled(Link)<Props>`
  text-decoration: none;
  color: #122434;
  width: ${props => (props.width ? props.width : undefined)};

  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`;
