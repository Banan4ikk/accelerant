import styled from 'styled-components';

export const StyledForm = styled.form``;

export const InputContainer = styled.div`
  display: block;
  &:first-child {
    margin-bottom: 30px;
  }
`;

export const StyledLabel = styled.label`
  color: #737373;
  font-size: 12px;
`;

export const CheckboxStyledLabel = styled.label`
  color: #737373;
  font-size: 12px;
  display: flex;
  align-items: center;
`;

export const ContinueContainer = styled.div`
  font-size: 12px;
  margin: 35px 0;
  color: #737373;
  text-align: center;
  @media (max-width: 375px) {
    margin: 15px 0;
  }
`;

export const SocialsImg = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

export const HideButton = styled.img`
  position: relative;
  top: -29px;
  left: 92%;
  &:hover {
    cursor: pointer;
  }
`;

export const AuthContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-bottom: 60px;
  @media (max-width: 375px) {
    margin-bottom: 40px;
  }
`;

export const StyledCheckbox = styled.input`
  width: 20px;
  height: 20px;
  margin-right: 10px;
`;

export const InsideContainer = styled.div`
  display: flex;
  width: 100%;
  padding: 0 7px;
  justify-content: space-between;
  align-items: center;
  font-weight: 300;
  font-size: 12px;
`;

export const StyledText = styled.div`
  color: #737373;
  &:hover {
    cursor: pointer;
    text-decoration: underline;
  }
`;

export const ErrorText = styled.p`
  color: #f05658;
  font-weight: 400;
  font-size: 12px;
`;
