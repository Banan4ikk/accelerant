import React, {useState} from 'react';
import {Field, Form} from 'react-final-form';
import {
  AuthContainer,
  CheckboxStyledLabel,
  ContinueContainer,
  ErrorText,
  HideButton,
  InputContainer,
  InsideContainer,
  SocialsImg,
  StyledCheckbox,
  StyledForm,
  StyledLabel,
  StyledText,
} from './styles';
import StyledInput from '../../../ui/StyledInput';
import {SubmitValues} from './types';
import FormHeader from '../../../components/FormHeader';
import SubmitButton from '../../../ui/SubmitButton';
import {signIn} from '../../../../redux/auth/thunks';
import {useDispatch} from 'react-redux';
import {emailPattern} from '../../../../constants';

const AuthForm = () => {
  const dispatch = useDispatch();
  const onSubmit = (values: SubmitValues) => {
    dispatch(signIn(values));
  };
  const [isHide, setIsHide] = useState(true);

  const onClickHide = () => {
    setIsHide(!isHide);
  };

  return (
    <Form
      onSubmit={onSubmit}
      validate={values => {
        const errors: SubmitValues = {} as SubmitValues;
        if (!new RegExp(emailPattern).test(values?.email)) {
          errors.email = 'Incorrect Email format';
        }
        if (!values.password) {
          errors.password = 'Required';
        }

        return errors;
      }}
      render={({handleSubmit, values}) => (
        <>
          <FormHeader />
          <StyledForm onSubmit={handleSubmit}>
            <InputContainer>
              <StyledLabel>
                Email
                <Field name="email" type="email">
                  {({meta, input}) => {
                    return (
                      <div>
                        <StyledInput input={input} meta={meta} />
                        {(meta.error || meta.submitError) && meta.touched && (
                          <ErrorText>
                            {meta.error || meta.submitError}
                          </ErrorText>
                        )}
                      </div>
                    );
                  }}
                </Field>
              </StyledLabel>
            </InputContainer>
            <InputContainer>
              <StyledLabel>
                Password
                <Field name="password" type={isHide ? 'password' : 'text'}>
                  {({meta, input}) => {
                    return (
                      <div>
                        <StyledInput input={input} meta={meta} />
                        {(meta.error || meta.submitError) && meta.touched && (
                          <ErrorText>
                            {meta.error || meta.submitError}
                          </ErrorText>
                        )}
                      </div>
                    );
                  }}
                </Field>
              </StyledLabel>
              <HideButton
                src={isHide ? '/images/hide.svg' : '/images/eye.svg'}
                onClick={onClickHide}
              />
            </InputContainer>
            <AuthContainer>
              <InsideContainer>
                <CheckboxStyledLabel>
                  <StyledCheckbox type="checkbox" />
                  Remember
                </CheckboxStyledLabel>
                <StyledText>Forgot password?</StyledText>
              </InsideContainer>
            </AuthContainer>
            <SubmitButton
              text="Login"
              disabled={Object.keys(values).length == 0}
            />
            <ContinueContainer>Or continue with</ContinueContainer>
            <SocialsImg>
              <img src={'/images/social.svg'} alt="logo" />
            </SocialsImg>
          </StyledForm>
        </>
      )}
    />
  );
};

export default AuthForm;
