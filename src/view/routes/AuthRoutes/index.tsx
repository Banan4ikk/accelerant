import React from 'react';
import RegisterForm from './RegisterForm';
import AuthWrapper from '../../ui/AuthWrapper';
import './authGlobalStyles.css';
import {Route, Routes} from 'react-router-dom';
import AuthForm from './AuthForm';
import {LOGIN_ROUTE, REGISTER_ROUTE} from './routes';

const AuthScreen = () => {
  return (
    <AuthWrapper>
      <Routes>
        <Route path={LOGIN_ROUTE} element={<AuthForm />} />
        <Route path={REGISTER_ROUTE} element={<RegisterForm />} />
      </Routes>
    </AuthWrapper>
  );
};

export default AuthScreen;
