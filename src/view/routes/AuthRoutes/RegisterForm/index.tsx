import React, {useState} from 'react';
import {Field, Form} from 'react-final-form';
import {
  ContinueContainer,
  HideButton,
  InputContainer,
  SocialsImg,
  StyledFlexContainer,
  StyledForm,
  StyledLabel,
  StyledTextContainer,
} from './styles';
import StyledInput from '../../../ui/StyledInput';
import {SubmitValues} from './types';
import FormHeader from '../../../components/FormHeader';
import SubmitButton from '../../../ui/SubmitButton';
import {signUp} from '../../../../redux/auth/thunks';
import {useDispatch} from 'react-redux';
import {emailPattern} from '../../../../constants';
import {ErrorText} from '../AuthForm/styles';

const RegisterForm = () => {
  const dispatch = useDispatch();
  const onSubmit = (values: SubmitValues) => {
    dispatch(signUp(values));
  };
  const [isHide, setIsHide] = useState(true);

  const onClickHide = () => {
    setIsHide(!isHide);
  };

  return (
    <Form
      onSubmit={onSubmit}
      validate={values => {
        const errors: SubmitValues = {} as SubmitValues;
        if (!new RegExp(emailPattern).test(values?.email)) {
          errors.email = 'Incorrect Email format';
        }
        if (!values.password) {
          errors.password = 'Required';
        }

        return errors;
      }}
      render={({handleSubmit, values}) => (
        <>
          <FormHeader />
          <StyledForm onSubmit={handleSubmit}>
            <InputContainer>
              <StyledLabel>Email</StyledLabel>
              <Field name="email" type="email">
                {({meta, input}) => {
                  return (
                    <div>
                      <StyledInput input={input} meta={meta} />
                      {(meta.error || meta.submitError) && meta.touched && (
                        <ErrorText>{meta.error || meta.submitError}</ErrorText>
                      )}
                    </div>
                  );
                }}
              </Field>
            </InputContainer>
            <InputContainer>
              <StyledLabel>Password</StyledLabel>
              <Field name="password" type={isHide ? 'password' : 'text'}>
                {({meta, input}) => {
                  return (
                    <div>
                      <StyledInput input={input} meta={meta} />
                      {(meta.error || meta.submitError) && meta.touched && (
                        <ErrorText>{meta.error || meta.submitError}</ErrorText>
                      )}
                    </div>
                  );
                }}
              </Field>
              <HideButton
                src={isHide ? '/images/hide.svg' : '/images/eye.svg'}
                onClick={onClickHide}
              />
            </InputContainer>
            <StyledFlexContainer>
              <StyledTextContainer>
                I agree that by clicking “Registration” I accept the Terms Of
                Service and Privacy Policy
              </StyledTextContainer>
            </StyledFlexContainer>

            <SubmitButton
              text="Registration"
              disabled={Object.keys(values).length == 0}
            />
            <ContinueContainer>Or continue with</ContinueContainer>
            <SocialsImg>
              <img src={'/images/social.svg'} alt="logo" />
            </SocialsImg>
          </StyledForm>
        </>
      )}
    />
  );
};

export default RegisterForm;
