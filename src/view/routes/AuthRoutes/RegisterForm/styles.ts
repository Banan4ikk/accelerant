import styled from 'styled-components';

export const StyledForm = styled.form``;

export const InputContainer = styled.div`
  display: block;
  &:first-child {
    margin-bottom: 30px;
  }
`;

export const StyledLabel = styled.label`
  color: #737373;
  font-size: 12px;
`;

export const StyledFlexContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

export const StyledTextContainer = styled.div`
  width: 100%;
  max-width: 35vw;
  height: fit-content;
  overflow-wrap: break-word;
  text-align: center;
  font-size: 12px;
  color: #737373;
  padding: 15px 5px;
`;

export const ContinueContainer = styled.div`
  font-size: 12px;
  margin: 35px 0;
  color: #737373;
  text-align: center;
  @media (max-width: 375px) {
    margin: 15px 0;
  }
`;

export const SocialsImg = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
`;

export const HideButton = styled.img`
  position: relative;
  top: -29px;
  left: 92%;
  &:hover {
    cursor: pointer;
  }
`;

export const InsideContainer = styled.div`
  display: flex;
  width: 100%;
  padding: 0 7px;
  justify-content: space-between;
  align-items: center;
  font-weight: 300;
  font-size: 12px;
`;

export const StyledText = styled.div`
  color: #737373;
  &:hover {
    cursor: pointer;
    text-decoration: underline;
  }
`;
