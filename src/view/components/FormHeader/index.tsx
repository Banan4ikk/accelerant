import React, {FC} from 'react';
import {
  HeaderContainer,
  NavigationContainer,
  StyledLinInactive,
  StyledLinkActive,
  Title,
} from './styles';
import {useLocation} from 'react-router-dom';
import {LOGIN_ROUTE, REGISTER_ROUTE} from '../../routes/AuthRoutes/routes';

const FormHeader: FC = () => {
  const {pathname} = useLocation();
  return (
    <HeaderContainer>
      <Title>Welcome to Accelerist</Title>
      <NavigationContainer>
        {pathname === '/register' ? (
          <StyledLinkActive to={REGISTER_ROUTE}>Register</StyledLinkActive>
        ) : (
          <StyledLinInactive to={REGISTER_ROUTE}>Register</StyledLinInactive>
        )}
        {pathname === '/login' ? (
          <StyledLinkActive to={LOGIN_ROUTE}>Login</StyledLinkActive>
        ) : (
          <StyledLinInactive to={LOGIN_ROUTE}>Login</StyledLinInactive>
        )}
      </NavigationContainer>
    </HeaderContainer>
  );
};

export default FormHeader;
