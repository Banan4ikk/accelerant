import styled from 'styled-components';
import {BorderedCardProps} from './types';

export const StyledContainer = styled.div<BorderedCardProps>`
  padding: 25px;
  display: flex;
  justify-content: center;
  align-items: center;
  width: ${props => props.width};
  height: fit-content;
  border: ${props => (props.border ? props.border : '1px #e8e8e8 solid')};
  border-radius: 6px;
`;
