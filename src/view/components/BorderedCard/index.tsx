import React, {FC} from 'react';
import {BorderedCardProps} from './types';
import {StyledContainer} from './styles';

const BorderedCard: FC<BorderedCardProps> = ({children, width, border}) => {
  return (
    <StyledContainer width={width} border={border}>
      {children}
    </StyledContainer>
  );
};

export default BorderedCard;
