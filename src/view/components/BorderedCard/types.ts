export type BorderedCardProps = {
  width: string | number;
  border?: string;
};
