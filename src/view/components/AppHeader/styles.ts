import styled from 'styled-components';

export const HeaderContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  height: 80px;
  width: 100vw;
  display: flex;
  align-items: center;
  justify-content: space-between;
  background: #d4f3ff;
  padding: 0 60px;
  @media (max-width: 375px) {
    width: 150vw;
  }
`;

export const FlexContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const MenuContainer = styled.div`
  display: flex;
  align-items: center;
  font-weight: 300;
  font-size: 12px;
  transform: translateY(4px);
  &:first-child {
    margin-left: 0;
  }
`;

export const MenuItem = styled.a`
  margin-left: 30px;
  height: fit-content;
  text-decoration: none;
  color: #122434;

  &:hover {
    cursor: pointer;
  }
  &:after {
    content: '';
    margin-top: 8px;
    position: relative;
    width: 0;
    display: flex;
    height: 2px;
    background: #122434;
    transition: width 0.2s ease-in-out;
  }
  &:hover::after {
    width: 100%;
  }
  &:link {
    text-decoration: none;
    color: #122434;
  }
`;

export const StyledSearchIcon = styled.img`
  position: relative;
  left: -160%;
  &:hover {
    cursor: pointer;
  }
`;

export const ProfileContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const StyledText = styled.p`
  margin-left: 15px;
  font-weight: 300;
  font-size: 12px;
  color: #122434;
`;
