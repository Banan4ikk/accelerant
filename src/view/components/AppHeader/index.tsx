import React from 'react';
import {
  FlexContainer,
  HeaderContainer,
  MenuContainer,
  MenuItem,
  ProfileContainer,
  StyledSearchIcon,
  StyledText,
} from './styles';
import SearchForm from '../SearchForm';
import {StyledLink} from '../../routes/UserRoutes/generalStyles';
import useWindowDimensions from '../../../utils/useWindowDemensios';
import {
  DASHBOARD_ROUTE,
  PROSPECTS_ROUTE,
  SEARCH_ROUTE,
} from '../../routes/UserRoutes/routes';

const AppHeader = () => {
  const {width} = useWindowDimensions();

  return (
    <HeaderContainer>
      <FlexContainer>
        <StyledLink to={DASHBOARD_ROUTE}>
          <img
            src={width <= 768 ? '/images/logoSmall.svg' : '/images/AppLogo.svg'}
            alt="logo"
          />
        </StyledLink>
        {width > 375 && (
          <MenuContainer>
            <MenuItem>
              <StyledLink to={DASHBOARD_ROUTE}>Dashboard</StyledLink>
            </MenuItem>
            <MenuItem>
              <StyledLink to={PROSPECTS_ROUTE}>Prospecting</StyledLink>
            </MenuItem>
          </MenuContainer>
        )}
      </FlexContainer>
      <FlexContainer>
        {width > 375 && <SearchForm />}
        <StyledLink to={SEARCH_ROUTE}>
          <StyledSearchIcon src="/images/search.svg" alt="search" />
        </StyledLink>
        <ProfileContainer>
          <img src="/images/profile-logo.svg" alt="profile logo" />
          <StyledText> United Nations</StyledText>
        </ProfileContainer>
      </FlexContainer>
    </HeaderContainer>
  );
};

export default AppHeader;
