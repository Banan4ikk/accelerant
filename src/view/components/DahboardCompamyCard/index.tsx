import React from 'react';

import DashboardFilterItem from '../../ui/DashboardFilterItem';
import NumbersItem from '../../ui/NumbersItem';
import DashboardCard from '../../ui/DashboardCard';
import {
  StyledCardTitle,
  StyledLink,
  StyledText,
  StyledTextBold,
  StyledTextGray,
} from '../../routes/UserRoutes/generalStyles';
import {
  ActivityContainer,
  FiltersContainer,
  FiltersTitle,
  Line,
  NumberCardContainer,
  OwnerAvatar,
  OwnerContainer,
  OwnerInfo,
  OwnerInfoContainer,
} from './styles';
import {SERVICES_ROUTE} from '../../routes/UserRoutes/routes';

const DashboardCompanyCard = () => {
  return (
    <DashboardCard>
      <StyledCardTitle>
        <StyledLink to={SERVICES_ROUTE}>Accounting Services</StyledLink>
        <Line />
      </StyledCardTitle>
      <FiltersTitle>Filters</FiltersTitle>
      <FiltersContainer>
        <DashboardFilterItem title="National" />
        <DashboardFilterItem title="$500-$1B" />
        <DashboardFilterItem title="Travel Industry" />
      </FiltersContainer>
      <NumberCardContainer>
        <NumbersItem
          title="№ of Prospects Available"
          number={234}
          background="#f9f9f9"
        />
        <NumbersItem
          title="№ of Contacts Pursued"
          number={73}
          background="#f9f9f9"
        />
      </NumberCardContainer>
      <OwnerContainer>
        <OwnerAvatar src="/images/ava.png" />
        <OwnerInfoContainer>
          <OwnerInfo>
            <StyledTextBold>Jenny Wilson</StyledTextBold>
            <StyledText>Owner</StyledText>
          </OwnerInfo>
          <ActivityContainer>
            <StyledTextGray>Last Activity</StyledTextGray>
            <StyledText>1 Jul 2020</StyledText>
          </ActivityContainer>
        </OwnerInfoContainer>
      </OwnerContainer>
    </DashboardCard>
  );
};

export default DashboardCompanyCard;
