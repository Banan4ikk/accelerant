import styled from 'styled-components';

export const Line = styled.div`
  width: 100%;
  margin-top: 10px;
  background: #e8e8e8;
  height: 1px;
`;

export const FiltersTitle = styled.p`
  color: #737373;
  font-weight: 400;
  font-size: 12px;
  margin: 15px 0;
`;

export const FiltersContainer = styled.div`
  display: flex;
  width: fit-content;
  justify-content: flex-start;
`;

export const OwnerContainer = styled.div`
  display: flex;
  margin-top: 25px;
`;

export const OwnerInfoContainer = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
`;

export const OwnerAvatar = styled.img`
  border-radius: 30px;
`;

export const OwnerInfo = styled.div`
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-direction: column;
  margin-left: 15px;
`;

export const ActivityContainer = styled.div`
  height: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: column;
  text-align: right;
`;

export const NumberCardContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;
