import styled from 'styled-components';

export const HeaderContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 80px;
  background: #122434;
  display: flex;
  justify-content: center;
  align-items: center;
`;
