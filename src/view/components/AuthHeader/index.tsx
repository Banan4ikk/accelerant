import React from 'react';
import {HeaderContainer} from './styles';

const AuthHeader = () => {
  return (
    <HeaderContainer>
      <img src={'/images/Logo.svg'} alt="logo" />
    </HeaderContainer>
  );
};

export default AuthHeader;
