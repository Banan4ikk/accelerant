import React, {FC, SetStateAction} from 'react';
import {Field, Form} from 'react-final-form';
import DashboardCard from '../../ui/DashboardCard';
import {
  StyledCardTitle,
  StyledTitle,
} from '../../routes/UserRoutes/generalStyles';
import {Container, InsideContainer, ItemContainer, StyledForm} from './styles';
import Select from 'react-select';
import {
  availableContributions,
  availableEthnicity,
  availableFocus,
  availableGoals,
  availableHousehold,
  availableIndustries,
  availableLocations,
  availableScope,
} from './constants';
import {
  CancelButton,
  StyledTextGray,
  SubmitButtonsContainer,
} from '../SearchCard/styles';
import 'rc-slider/assets/index.css';

import FilterItemsButtons from '../FilterItemsButtons';
import TooltipSlider from '../HandleTooltip';
import SubmitButton from '../../ui/SubmitButton';
import {submitValues} from './types';
import {filerCompanies} from '../../../redux/company/thunks';
import {useDispatch} from 'react-redux';

const FiltersForm: FC<{
  setIsShowFilters: React.Dispatch<SetStateAction<boolean>>;
}> = ({setIsShowFilters}) => {
  const dispatch = useDispatch();

  const onSubmit = (values: submitValues) => {
    setIsShowFilters(false);
    dispatch(filerCompanies({householdIncome: values.household}));
  };

  return (
    <Form
      onSubmit={onSubmit}
      render={({handleSubmit}) => (
        <StyledForm onSubmit={handleSubmit}>
          <DashboardCard width="100%" marginTop="140px">
            <StyledTitle>Filters</StyledTitle>
            <StyledCardTitle>Company</StyledCardTitle>
            <Container>
              <InsideContainer>
                <ItemContainer>
                  <StyledTextGray>Industry</StyledTextGray>
                  <Field name="industry" placeholder="Search">
                    {({input}) => (
                      <Select
                        options={availableIndustries}
                        placeholder="Select"
                        {...input}
                        isMulti
                        isClearable
                      />
                    )}
                  </Field>
                </ItemContainer>
                <ItemContainer>
                  <StyledTextGray>Scope</StyledTextGray>
                  <Field name="scope" placeholder="Search">
                    {({input}) => (
                      <Select
                        options={availableScope}
                        placeholder="Select"
                        {...input}
                        isClearable
                      />
                    )}
                  </Field>
                </ItemContainer>
                <ItemContainer>
                  <StyledTextGray>CDR Focus</StyledTextGray>
                  <Field name="focus" placeholder="Search">
                    {({input}) => (
                      <Select
                        options={availableFocus}
                        placeholder="Select"
                        {...input}
                        isClearable
                      />
                    )}
                  </Field>
                </ItemContainer>
                <ItemContainer>
                  <StyledTextGray>Revenue</StyledTextGray>
                  <Field name="revenue">
                    {({input}) => (
                      <TooltipSlider
                        range
                        min={5}
                        max={150}
                        step={1}
                        allowCross={false}
                        defaultValue={[15, 50]}
                        tipFormatter={value => `$${value}M`}
                        {...input}
                      />
                    )}
                  </Field>
                </ItemContainer>
              </InsideContainer>
              <InsideContainer>
                <ItemContainer>
                  <StyledTextGray>Geographic Location</StyledTextGray>
                  <Field name="location" placeholder="Search">
                    {({input}) => (
                      <Select
                        options={availableLocations}
                        placeholder="Select"
                        {...input}
                        isMulti
                        isClearable
                      />
                    )}
                  </Field>
                </ItemContainer>
                <ItemContainer>
                  <StyledTextGray>SDG Goals</StyledTextGray>
                  <Field name="goals" placeholder="Search">
                    {({input}) => (
                      <Select
                        options={availableGoals}
                        placeholder="Select"
                        {...input}
                        isClearable
                      />
                    )}
                  </Field>
                </ItemContainer>
                <ItemContainer>
                  <StyledTextGray>Total Annual Contributions</StyledTextGray>
                  <Field name="contributions" placeholder="Search">
                    {({input}) => (
                      <Select
                        options={availableContributions}
                        placeholder="Select"
                        {...input}
                        isClearable
                      />
                    )}
                  </Field>
                </ItemContainer>
              </InsideContainer>
            </Container>
            <StyledCardTitle>Customer Demography</StyledCardTitle>
            <Container>
              <InsideContainer>
                <StyledTextGray>Gender</StyledTextGray>
                <Field name="gender">
                  {() => (
                    <FilterItemsButtons items={['Male', 'Female', 'Both']} />
                  )}
                </Field>
                <StyledTextGray>Household income</StyledTextGray>
                <Field name="household" placeholder="Search">
                  {({input}) => (
                    <Select
                      options={availableHousehold}
                      placeholder="Select"
                      {...input}
                      isClearable
                    />
                  )}
                </Field>
                <StyledTextGray marginTop="35px" marginBottom="25px">
                  Age
                </StyledTextGray>
                <Field name="revenue">
                  {() => (
                    <TooltipSlider
                      range
                      min={18}
                      max={75}
                      step={1}
                      allowCross={false}
                      defaultValue={[30, 40]}
                      tipFormatter={value => `${value}`}
                    />
                  )}
                </Field>
                <SubmitButtonsContainer>
                  <CancelButton>Cancel</CancelButton>
                  <SubmitButton
                    text="Search"
                    width="30%"
                    marginTop="0"
                    fontWeight="400"
                  />
                </SubmitButtonsContainer>
              </InsideContainer>
              <InsideContainer>
                <InsideContainer>
                  <StyledTextGray>Relations</StyledTextGray>
                  <Field name="relations">
                    {() => <FilterItemsButtons items={['Single', 'Married']} />}
                  </Field>
                </InsideContainer>
                <ItemContainer>
                  <StyledTextGray>Ethnicity</StyledTextGray>
                  <Field name="ethnicity" placeholder="Search">
                    {({input}) => (
                      <Select
                        options={availableEthnicity}
                        placeholder="Select"
                        {...input}
                        isClearable
                      />
                    )}
                  </Field>
                </ItemContainer>
              </InsideContainer>
            </Container>
          </DashboardCard>
        </StyledForm>
      )}
    />
  );
};

export default FiltersForm;
