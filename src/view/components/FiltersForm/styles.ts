import styled from 'styled-components';

export const StyledForm = styled.form`
  display: flex;
  width: 100%;
  justify-content: space-between;
  align-items: center;
`;

export const Container = styled.div`
  height: fit-content;
  display: flex;
  margin-bottom: 50px;
  &:last-child {
    margin-bottom: 0;
  }
`;

export const InsideContainer = styled.div`
  width: 50%;
  height: fit-content;
  margin-right: 25px;
`;

export const ItemContainer = styled.div`
  &:first-child {
    margin-top: 0;
  }
  width: 100%;
  height: 100%;
  margin-top: 30px;
`;
