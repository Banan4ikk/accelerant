export const availableAgeRanges = [
  {value: '18-20', label: '18-20'},
  {value: '21-25', label: '21-25'},
  {value: '26-30', label: '26-30'},
  {value: '31-35', label: '31-35'},
  {value: '36-40', label: '36-40'},
  {value: '41-45', label: '41-45'},
  {value: '46-50', label: '46-50'},
  {value: '51-55', label: '51-55'},
  {value: '56-60', label: '56-60'},
  {value: '61-65', label: '61-65'},
  {value: '66-70', label: '66-70'},
  {value: '71-75', label: '71-75'},
  {value: '75+', label: '75+'},
];

export const availableHousehold = [
  {value: 'Less%20than%20%2420K', label: 'Less then $20K'},
  {value: '%2420K%20-%20%2429K', label: '$20K-$29K'},
  {value: '%2430K%20-%20%2439K', label: '$30K-$39K'},
  {value: '%2440K%20-%20%2449K', label: '$40K-$49K'},
  {value: '%2450K%20-%20%2474K', label: '$50K-$74K'},
  {value: '%2475K%20-%20%2499K', label: '$75K-$99K'},
  {value: '%24100K%20-%20%24124K', label: '$100K-$124K'},
  {value: '%24125K%20or%20More', label: '$125K or More'},
];

export const availableIndustries = [
  {value: 'Agriculture', label: 'Agriculture'},
  {
    value: 'Entertainment and Recreation',
    label: 'Entertainment and Recreation',
  },
  {value: 'Bars and Food Services', label: 'Bars and Food Services'},
  {value: 'Health', label: 'Health'},
];

export const availableScope = [
  {value: 'local', label: 'Local'},
  {value: 'foreign', label: 'Foreign'},
  {value: 'both', label: 'Both'},
];

export const availableFocus = [
  {value: 'Health', label: 'Health'},
  {value: 'Animals', label: 'Animals'},
  {value: 'Auto', label: 'Auto'},
  {value: 'Medicine', label: 'Medicine'},
  {value: 'Environment', label: 'Environment'},
];

export const availableHouseholdIncome = [
  {value: 'Highest', label: 'Highest'},
  {value: 'Higher', label: 'Higher'},
  {value: 'Lower', label: 'Lower'},
  {value: 'Lowermost', label: 'Lowermost'},
];

export const availableLocations = [
  {value: 'Alabama', label: 'Alabama'},
  {value: 'Missouri', label: 'Missouri'},
  {value: 'Alaska', label: 'Alaska'},
  {value: 'Asia', label: 'Asia'},
  {value: 'Europe', label: 'Europe'},
];

export const availableGoals = [
  {value: 'Goal1', label: 'Goal 1'},
  {value: 'Goal2', label: 'Goal 2'},
  {value: 'Goal3', label: 'Goal 3'},
];

export const availableContributions = [
  {value: 'Contribution1', label: 'Contribution 1'},
  {value: 'Contribution2', label: 'Contribution 2'},
  {value: 'Contribution3', label: 'Contribution 3'},
];

export const availableEthnicity = [
  {value: 'American', label: 'American'},
  {value: 'Canadian', label: 'Canadian'},
  {value: 'Russian', label: 'Russian'},
  {value: 'Chinese', label: 'Chinese'},
];
