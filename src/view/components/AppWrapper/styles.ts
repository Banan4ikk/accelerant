import styled from 'styled-components';

export const Container = styled.div`
  background: #f9f9f9;
  padding: 60px;
  width: 100vw;
  min-height: 120vh;
  height: fit-content;
  @media (max-width: 375px) {
    width: 150vw;
  }
`;
