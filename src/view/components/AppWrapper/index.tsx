import React, {FC} from 'react';
import AppHeader from '../AppHeader';
import {Container} from './styles';

const AppWrapper: FC = ({children}) => {
  return (
    <Container>
      <AppHeader />
      {children}
    </Container>
  );
};

export default AppWrapper;
