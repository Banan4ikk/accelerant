import React from 'react';
import {Field, Form} from 'react-final-form';
import {StyledForm} from './styles';
import StyledInput from '../../ui/StyledInput';

const SearchForm = () => {
  const onSubmit = () => {
    console.log();
  };
  return (
    <Form
      onSubmit={onSubmit}
      render={() => (
        <StyledForm>
          <Field name="search" placeholder="Search" component={StyledInput} />
        </StyledForm>
      )}
    />
  );
};

export default SearchForm;
