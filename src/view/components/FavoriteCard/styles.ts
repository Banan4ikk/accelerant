import styled from 'styled-components';

export const FavoriteCardContainer = styled.div`
  width: 48%;
  background: #fff;
  height: fit-content;
  padding: 25px;
  border-radius: 6px;
  margin-bottom: 15px;
  display: flex;
  flex-direction: column;
`;

export const CardInfo = styled.div`
  width: 50px;
  height: 50px;
  border-radius: 6px;
`;

export const CompanyInfo = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;
  text-align: left;
  height: fit-content;
  margin-bottom: 20px;
  padding: 7px 0;
  margin-left: 10px;
`;

export const InfoContainer = styled.div`
  display: flex;
`;

export const StyledFavoriteTitle = styled.p`
  font-size: 12px;
  font-weight: 500;
  color: #122434;
`;

export const StyledFavoriteRanking = styled.p`
  font-size: 12px;
  font-weight: 400;
  color: #737373;
`;

export const CompanyFocus = styled.div`
  width: 100%;
  height: 40px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;

export const FocusText = styled.p`
  color: #737373;
  font-size: 12px;
  font-weight: 400;
`;

export const FocusItemsContainer = styled.div`
  display: flex;
  width: 80%;
  justify-content: space-between;
`;

export const FocusItem = styled.div`
  color: #122434;
  font-size: 12px;
  font-weight: 500;
`;
