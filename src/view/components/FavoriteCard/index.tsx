import React, {FC} from 'react';
import {StyledTextBold} from '../../routes/UserRoutes/generalStyles';
import {
  CardInfo,
  CompanyFocus,
  CompanyInfo,
  FavoriteCardContainer,
  FocusItemsContainer,
  FocusText,
  InfoContainer,
  StyledFavoriteRanking,
  StyledFavoriteTitle,
} from './styles';
import {useSelector} from 'react-redux';
import {selectors} from '../../../redux/ducks';

const FavoriteCard: FC<{id: string}> = ({id: companyId}) => {
  const data = useSelector(selectors.favorites.selectCompanyData(companyId));

  const {name, logo, score} = data;

  return (
    <FavoriteCardContainer>
      <InfoContainer>
        <CardInfo>
          <img
            src={logo == null ? '/images/empty-logo-mini.svg' : logo}
            alt="card logo"
          />
        </CardInfo>
        <CompanyInfo>
          <StyledFavoriteTitle>{name}</StyledFavoriteTitle>
          <StyledFavoriteRanking>
            Priority Ranking {score}
          </StyledFavoriteRanking>
        </CompanyInfo>
      </InfoContainer>
      <CompanyFocus>
        <FocusText>CSR Focus</FocusText>
        <FocusItemsContainer>
          <StyledTextBold>No information</StyledTextBold>
        </FocusItemsContainer>
      </CompanyFocus>
    </FavoriteCardContainer>
  );
};

export default FavoriteCard;
