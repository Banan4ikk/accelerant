import {CSSProperties} from 'react';

export const handleStyle: CSSProperties = {
  padding: '5px',
  border: '1px #000 solid',
  borderRadius: '4px',
};
