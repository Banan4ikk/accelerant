import styled from 'styled-components';
import {Link} from 'react-router-dom';

export const HeaderContainer = styled.div`
  height: 90px;
  width: 100%;
  text-align: center;
  margin-bottom: 35px;
  display: flex;
  justify-content: center;
  flex-direction: column;
`;

export const Title = styled.div`
  font-size: 24px;
  font-weight: 500;
  margin-bottom: 25px;
`;

export const NavigationContainer = styled.div`
  width: 100%;
  height: 40px;
  margin-bottom: 30px;
  border-radius: 6px;
  padding: 3px;
  display: flex;
  min-width: 35vw;
  background-color: #f8f8f8;
  justify-content: center;
  @media (max-width: 768px) {
    min-width: 50vw;
  }
`;

export const StyledActive = styled.div`
  width: 50%;
  height: 100%;
  border-radius: 6px;
  background-color: #caf0ff;
  display: flex;
  justify-content: center;
  align-items: center;
  &:hover {
    cursor: pointer;
  }
`;

export const StyledInactive = styled.div`
  width: 50%;
  height: 100%;
  border-radius: 6px;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #737373;
  transition: color 0.2s ease-in-out;
  font-size: 12px;
  font-weight: 400;
  &:hover {
    color: #122434;
    cursor: pointer;
  }
`;
