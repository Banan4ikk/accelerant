import React, {FC, useState} from 'react';
import {NavigationContainer, StyledActive, StyledInactive} from './styles';
import {ButtonsProps} from './types';
import {StyledText} from '../../routes/UserRoutes/generalStyles';

const FilterItemsButtons: FC<ButtonsProps> = ({items}) => {
  const [activeIndex, setActiveIndex] = useState(0);

  return (
    <NavigationContainer>
      {items.map((item, index) =>
        index === activeIndex ? (
          <StyledActive>
            <StyledText>{item}</StyledText>
          </StyledActive>
        ) : (
          <StyledInactive onClick={() => setActiveIndex(index)}>
            {item}
          </StyledInactive>
        ),
      )}
    </NavigationContainer>
  );
};

export default FilterItemsButtons;
