import React, {FC, useEffect, useState} from 'react';
import DashboardCard from '../../ui/DashboardCard';
import {
  ButtonsContainer,
  FlexContainer,
  FocusContainer,
  InfoBottomContainer,
  InfoContainer,
  InfoHeaderContainer,
  LikeContainer,
  LogoContainer,
  LogoImage,
  PriorityContainer,
  ProfileButton,
  RevenueContainer,
  SearchInfoContainer,
  StyledCardTitle,
  StyledInfoTitle,
  StyledText,
  StyledTextBold,
  StyledTextGray,
  TextContainer,
} from './styles';
import useWindowDimensions from '../../../utils/useWindowDemensios';
import {useDispatch, useSelector} from 'react-redux';
import {dislikeCompany, likeCompany} from '../../../redux/company/thunks';
import {selectors} from '../../../redux/ducks';
import {useNavigate} from 'react-router-dom';
import {PROFILE_ROUTE} from '../../routes/UserRoutes/routes';

const SearchCard: FC<{companyId: string; favorite?: boolean}> = ({
  companyId,
  favorite,
}) => {
  const {width} = useWindowDimensions();
  const data = favorite
    ? useSelector(selectors.favorites.selectCompanyData(companyId))
    : useSelector(selectors.corporate.selectCompanyData(companyId));
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const [isLiked, setIsLiked] = useState<boolean | undefined>(false);

  const {
    id,
    name,
    zipCode,
    city = '',
    state = '',
    street,
    country,
    phone,
    revenue,
    like,
    logo,
    score,
  } = data;

  const getImg = (): string => {
    let Logo;
    if (width <= 768) {
      if (logo == null) {
        Logo = 'images/empty-logo-screen2.svg';
      } else {
        Logo = logo;
      }
    } else {
      if (logo == null) {
        Logo = 'images/empty-logo.svg';
      } else {
        Logo = logo;
      }
    }
    return Logo;
  };

  const onLike = () => {
    if (like) {
      setIsLiked(false);
      dispatch(dislikeCompany(companyId));
    } else {
      setIsLiked(true);
      dispatch(likeCompany(companyId));
    }
  };

  const onClickProfile = () => {
    navigate(PROFILE_ROUTE, {state: {id}});
  };

  useEffect(() => {
    setIsLiked(like);
  }, []);

  return (
    <DashboardCard>
      <SearchInfoContainer>
        <FlexContainer>
          <LogoContainer>
            <LogoImage>
              <img src={getImg()} alt="card logo" />
            </LogoImage>
            <PriorityContainer>
              <StyledTextGray>Priority Ranking</StyledTextGray>
              <StyledCardTitle>{score}</StyledCardTitle>
            </PriorityContainer>
          </LogoContainer>
          <TextContainer>
            <InfoContainer>
              <InfoHeaderContainer>
                <StyledInfoTitle>{name}</StyledInfoTitle>
                <StyledTextGray>
                  {street} {city} {state} {country} {zipCode}
                </StyledTextGray>
                <StyledTextGray>{phone}</StyledTextGray>
              </InfoHeaderContainer>
              <InfoBottomContainer>
                <FocusContainer>
                  <StyledTextGray>CSR Focus</StyledTextGray>
                  <StyledTextBold>No information</StyledTextBold>
                </FocusContainer>
                <RevenueContainer>
                  <StyledTextGray>Revenue</StyledTextGray>
                  <StyledTextBold>${revenue}</StyledTextBold>
                </RevenueContainer>
              </InfoBottomContainer>
            </InfoContainer>
            {width > 768 ? (
              <ButtonsContainer>
                <LikeContainer onClick={onLike}>
                  <img
                    src={
                      isLiked ? '/images/filled-heart.svg' : '/images/like.svg'
                    }
                  />
                </LikeContainer>
                <ProfileButton onClick={onClickProfile}>
                  <StyledText>Profile</StyledText>
                </ProfileButton>
              </ButtonsContainer>
            ) : null}
          </TextContainer>
        </FlexContainer>
        {width <= 768 ? (
          <ButtonsContainer>
            <LikeContainer>
              <img src="/images/like.svg" />
            </LikeContainer>
            <ProfileButton>
              <StyledText>Profile</StyledText>
            </ProfileButton>
          </ButtonsContainer>
        ) : null}
      </SearchInfoContainer>
    </DashboardCard>
  );
};

export default SearchCard;
