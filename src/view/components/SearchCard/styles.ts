import styled from 'styled-components';

type Props = {
  marginTop?: string;
  marginBottom?: string;
};

export const SearchInfoContainer = styled.div`
  display: flex;
  flex-direction: column;
  @media (max-width: 768px) {
    justify-content: space-between;
  }
`;

export const FlexContainer = styled.div`
  display: flex;
  margin-bottom: 7px;
`;

export const LogoContainer = styled.div`
  width: fit-content;
  height: fit-content;
  border-radius: 6px;
  border: 1px #e8e8e8 solid;
  margin-right: 15px;
`;

export const LogoImage = styled.div`
  @media (max-width: 768px) {
    padding: 20px;
  }
  padding: 40px;
`;

export const PriorityContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 10px;
  border-top: 1px #e8e8e8 solid;
  @media (max-width: 768px) {
    border: none;
  }
`;

export const ButtonsContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

export const StyledTextGray = styled.p<Props>`
  color: #737373;
  font-size: 12px;
  font-weight: 400;
  margin-bottom: ${props => (props.marginBottom ? props.marginBottom : '7px')};
  margin-top: ${props => (props.marginTop ? props.marginTop : undefined)};
  @media (max-width: 768px) {
    margin-bottom: 5px;
  }
`;

export const StyledCardTitle = styled.p`
  color: #122434;
  font-size: 16px;
  font-weight: 500;
`;

export const TextContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-direction: column;
`;

export const InfoContainer = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding-bottom: 15px;
  height: 186px;
  border-bottom: 1px #e8e8e8 solid;
`;

export const InfoHeaderContainer = styled.div``;

export const StyledInfoTitle = styled.p`
  color: #122434;
  font-size: 16px;
  font-weight: 500;
  margin-bottom: 10px;
`;

export const InfoBottomContainer = styled.div`
  display: flex;
  justify-content: space-between;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

export const FocusContainer = styled.div`
  width: fit-content;
  padding-right: 20px;
  border-right: 1px #e8e8e8 solid;
  display: flex;
  flex-direction: column;
  @media (max-width: 768px) {
    border: none;
    margin-bottom: 5px;
  }
`;

export const StyledTextBold = styled.p`
  color: #122434;
  font-size: 12px;
  font-weight: 500;
  @media (max-width: 768px) {
    font-size: 11px;
    margin-left: 4px;
  }
`;

export const RevenueContainer = styled.div`
  width: fit-content;
  display: flex;
  flex-direction: column;
  text-align: right;
  @media (max-width: 768px) {
    flex-direction: row;
  }
`;

export const LikeContainer = styled.div`
  padding: 10px;
  width: fit-content;
  border-radius: 6px;
  border: 1px #e8e8e8 solid;
  margin-right: 10px;
  transition: border-color 0.2s ease-in-out;

  &:hover {
    cursor: pointer;
    border-color: #bfbfbf;
  }
`;

export const ProfileButton = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  border-radius: 6px;
  padding: 3% 5%;
  border: 1px #2baee0 solid;
  transition: background 0.2s ease-in-out;

  &:hover {
    cursor: pointer;
    background: #ebf9ff;
  }
`;

export const StyledText = styled.p`
  color: #122434;
  font-size: 12px;
  font-weight: 400;
`;

export const SubmitButtonsContainer = styled.div`
  display: flex;
  margin-top: 50px;
  width: 100%;
  justify-content: flex-start;
  align-items: center;
`;

export const CancelButton = styled.button`
  width: 30%;
  text-align: center;
  padding: 3%;
  background: none;
  border-radius: 6px;
  color: #122434;
  font-weight: 400;
  font-size: 16px;
  border: 1px #e8e8e8 solid;
  margin-right: 10px;
  transition: border-color 0.2s ease-in-out;

  &:hover {
    cursor: pointer;
    border-color: #bfbfbf;
  }
`;
