import React, {FC} from 'react';
import {NumbersContainer, StyledNumber, StyledTitle} from './styles';
import {NumberCardProps} from './types';

const NumbersItem: FC<NumberCardProps> = ({
  title,
  number,
  background,
  border,
}) => {
  return (
    <NumbersContainer background={background} border={border}>
      <StyledTitle>{title}</StyledTitle>
      <StyledNumber>{number}</StyledNumber>
    </NumbersContainer>
  );
};

export default NumbersItem;
