import styled from 'styled-components';

type Props = {
  background: string;
  border: string | undefined;
};

export const NumbersContainer = styled.div<Props>`
  width: 50%;
  background: ${props => props.background};
  border: ${props => props.border};
  border-radius: 4px;
  display: flex;
  justify-content: center;
  flex-direction: column;
  align-items: center;
  margin-top: 30px;
  padding: 5px;
`;

export const StyledTitle = styled.p`
  font-weight: 400;
  font-size: 12px;
  color: #737373;
`;

export const StyledNumber = styled.div`
  font-size: 24px;
  font-weight: 500;
  color: #122434;
  padding: 7px;
  margin-top: 5px;
`;
