export type NumberCardProps = {
  title: string;
  number: string | number;
  background: string;
  border?: string;
};

export type BorderVars = {
  width: number;
  color: string;
  type: string;
};
