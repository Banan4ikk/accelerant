import styled from 'styled-components';

export const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  margin-top: 9em;
  justify-content: center;
  align-items: center;
  padding: 12vh 0;
  @media (max-width: 375px) {
    padding: 20px;
  }
`;

export const FormContainer = styled.div`
  width: fit-content;
  height: fit-content;
  padding: 2em;
  background: #fff;
  border-radius: 6px;
`;
