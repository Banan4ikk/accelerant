import React, {FC, useEffect} from 'react';
import AuthHeader from '../../components/AuthHeader';
import {Container, FormContainer} from './styles';
import {useNavigate} from 'react-router-dom';

const AuthWrapper: FC = ({children}) => {
  const navigate = useNavigate();

  useEffect(() => {
    navigate('/login');
  }, []);

  return (
    <Container>
      <AuthHeader />
      <FormContainer>{children}</FormContainer>
    </Container>
  );
};

export default AuthWrapper;
