import styled from 'styled-components';

export const Input = styled.input`
  width: 100%;
  height: 35px;
  border-radius: 6px;
  border: 1px #e8e8e8 solid;
  font-size: 14px;
  padding: 5px 15px;

  &:focus {
    border-color: #2baee0;
  }
`;

export const ErrorInput = styled.input`
  width: 100%;
  height: 35px;
  border-radius: 6px;
  border: 1px #f05658 solid;
  background: #fff2f2;
  font-size: 14px;
  padding: 5px 15px;

  &:focus {
    border: 1px #f05658 solid;
  }
`;
