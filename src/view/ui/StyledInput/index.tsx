import React, {FC} from 'react';
import {ErrorInput, Input} from './styles';
import {FieldRenderProps} from 'react-final-form';

type InputProps = FieldRenderProps<string>;

const StyledInput: FC<InputProps> = ({input, placeholder, meta}) => {
  return (meta.error || meta.submitError) && meta.touched ? (
    <ErrorInput {...input} placeholder={placeholder} />
  ) : (
    <Input {...input} placeholder={placeholder} />
  );
};

export default StyledInput;
