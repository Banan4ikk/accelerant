import React, {FC} from 'react';
import {FilterContainer, StyledText} from './styles';
import {FilterProps} from './types';

const DashboardFilterItem: FC<FilterProps> = ({title}) => {
  return (
    <FilterContainer>
      <StyledText>{title}</StyledText>
    </FilterContainer>
  );
};

export default DashboardFilterItem;
