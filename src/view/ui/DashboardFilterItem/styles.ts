import styled from 'styled-components';

export const FilterContainer = styled.div`
  padding: 7px;
  border-radius: 6px;
  border: 1px #d4f3ff solid;
  width: fit-content;
  margin-right: 5px;
  background: #fff;
  &:first-child {
    margin-left: 15px;
  }
`;

export const StyledText = styled.p`
  font-weight: 400;
  font-size: 12px;
  color: #122434;
`;
