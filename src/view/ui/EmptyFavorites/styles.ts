import styled from 'styled-components';

type Props = {
  width: string;
  height: string;
};

export const FavoritesContainer = styled.div<Props>`
  width: ${props => `${props.width}`};
  height: ${props => `${props.height}`};
  border-radius: 6px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: #fff;
`;

export const StyledImage = styled.img`
  margin: 15px;
`;

export const InsideContainer = styled.div`
  width: fit-content;
  height: fit-content;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
`;

export const StyledButton = styled.div`
  display: flex;
  justify-content: center;
  margin-top: 30px;
  width: 100%;
  padding: 10px 0;
  border-radius: 6px;
  border: 1px #2baee0 solid;
  &:hover {
    cursor: pointer;
  }
`;
