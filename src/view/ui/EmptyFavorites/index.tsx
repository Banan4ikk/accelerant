import React, {FC} from 'react';
import {
  StyledCardTitle,
  StyledLink,
  StyledText,
  StyledTextGray,
} from '../../routes/UserRoutes/generalStyles';
import {
  FavoritesContainer,
  InsideContainer,
  StyledButton,
  StyledImage,
} from './styles';
import {EmptyFavoritesProps} from './types';
import {SEARCH_ROUTE} from '../../routes/UserRoutes/routes';

const EmptyFavorites: FC<EmptyFavoritesProps> = ({width, height}) => {
  return (
    <FavoritesContainer width={width} height={height}>
      <InsideContainer>
        <StyledImage src="/images/heartGray.svg" />
        <StyledCardTitle>No favorite company</StyledCardTitle>
        <StyledTextGray>
          Go to the search page and add to favorites
        </StyledTextGray>
        <StyledLink width="100%" to={SEARCH_ROUTE}>
          <StyledButton>
            <StyledText>Search</StyledText>
          </StyledButton>
        </StyledLink>
      </InsideContainer>
    </FavoritesContainer>
  );
};

export default EmptyFavorites;
