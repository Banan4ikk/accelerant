import React, {FC} from 'react';
import {DisabledButton, StyledButton} from './styles';
import {ButtonProps} from './types';

const SubmitButton: FC<ButtonProps> = props => {
  return props.disabled ? (
    <DisabledButton {...props}>{props.text}</DisabledButton>
  ) : (
    <StyledButton type="submit" {...props}>
      {props.text}
    </StyledButton>
  );
};

export default SubmitButton;
