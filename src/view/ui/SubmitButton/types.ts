export type ButtonProps = {
  text: string;
  disabled?: boolean;
  width?: string;
  padding?: string;
  marginTop?: string;
  fontWeight?: string;
};
