import styled from 'styled-components';

type Props = {
  width?: string;
  padding?: string;
  marginTop?: string;
  fontWeight?: string;
};

export const StyledButton = styled.button<Props>`
  border: none;
  width: ${props => (props.width ? props.width : '100%')};
  text-align: center;
  padding: ${props => (props.padding ? props.padding : '3%')};
  margin-top: ${props => (props.marginTop ? props.marginTop : '15px')};
  border-radius: 6px;
  background-color: #2baee0;
  color: #fff;
  font-weight: ${props => (props.fontWeight ? props.fontWeight : '500')};
  font-size: 16px;
  transition: background 0.1s ease-in-out;

  &:hover {
    cursor: pointer;
    background: #51c2ee;
  }
`;
export const DisabledButton = styled.button<Props>`
  border: none;
  width: ${props => (props.width ? props.width : '100%')};
  text-align: center;
  padding: ${props => (props.padding ? props.padding : '3%')};
  margin-top: ${props => (props.marginTop ? props.marginTop : '15px')};
  border-radius: 6px;
  background-color: #ceedf9;
  color: #2baef0;
  font-weight: ${props => (props.fontWeight ? props.fontWeight : '500')};
  font-size: 16px;
  pointer-events: none;
`;
