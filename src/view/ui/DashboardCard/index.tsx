import React, {FC} from 'react';
import {CardContainer} from './styles';

type Props = {
  width?: string;
  marginTop?: string;
};

const DashboardCard: FC<Props> = ({children, width, marginTop}) => {
  return (
    <CardContainer width={width} marginTop={marginTop}>
      {children}
    </CardContainer>
  );
};

export default DashboardCard;
