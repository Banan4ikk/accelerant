import styled from 'styled-components';

type Props = {
  width?: string;
  marginTop?: string;
};

export const CardContainer = styled.div<Props>`
  padding: 25px;
  width: ${props => (props.width ? props.width : '45%')};
  border-radius: 6px;
  height: fit-content;
  background: #fff;
  margin-top: ${props => props.marginTop && props.marginTop};
  margin-right: 25px;
  margin-bottom: 25px;
  @media (max-width: 375px) {
    width: 100%;
  }
`;
