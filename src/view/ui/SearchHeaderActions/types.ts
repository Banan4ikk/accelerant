export type HeaderActionsProps = {
  imgPath: string;
  title: string;
};
