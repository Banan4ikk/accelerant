import styled from 'styled-components';

export const StyledContainer = styled.div`
  display: flex;
  align-items: center;
  margin-right: 40px;
  @media (max-width: 375px) {
    margin-right: 10px;
  }
`;

export const StyledImg = styled.img`
  margin-right: 10px;
`;

export const StyledText = styled.p`
  color: #122434;
  font-size: 12px;
  font-weight: 400;
`;
