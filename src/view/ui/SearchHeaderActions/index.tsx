import React, {FC} from 'react';
import {StyledContainer, StyledImg, StyledText} from './styles';
import {HeaderActionsProps} from './types';

const SearchHeaderActions: FC<HeaderActionsProps> = ({imgPath, title}) => {
  return (
    <StyledContainer>
      <StyledImg src={imgPath} />
      <StyledText>{title}</StyledText>
    </StyledContainer>
  );
};

export default SearchHeaderActions;
