import styled from 'styled-components';

type Props = {
  spaceBetween?: boolean;
};

export const Container = styled.div<Props>`
  position: absolute;
  left: 0;
  top: 80px;
  height: 100px;
  width: 100vw;
  background: #fff;
  padding: 25px 60px;
  display: flex;
  justify-content: ${props =>
    props.spaceBetween ? 'space-between' : 'flex-start'};
  align-items: center;
  @media (max-width: 375px) {
    width: 150vw;
  }
`;

export const StyledTitle = styled.h1`
  font-weight: 500;
  font-size: 32px;
  margin-right: 80px;
`;

export const StyledBack = styled.img`
  margin-right: 25px;
  &:hover {
    cursor: pointer;
  }
`;
