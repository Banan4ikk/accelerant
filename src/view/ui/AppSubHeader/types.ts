export type SubHeaderProps = {
  title: string;
  isBack?: boolean;
  spaceBetween?: boolean;
};
