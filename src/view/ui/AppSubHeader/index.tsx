import React, {FC} from 'react';
import {Container, StyledBack, StyledTitle} from './styles';
import {SubHeaderProps} from './types';
import {useNavigate} from 'react-router-dom';

const AppSubHeader: FC<SubHeaderProps> = ({
  children,
  title,
  isBack,
  spaceBetween,
}) => {
  const navigate = useNavigate();

  const onClickBack = () => {
    navigate(-1);
  };

  return (
    <Container spaceBetween={spaceBetween}>
      {isBack && (
        <StyledBack src="/images/leftArrow.svg" onClick={onClickBack} />
      )}
      <StyledTitle>{title}</StyledTitle>
      {children}
    </Container>
  );
};

export default AppSubHeader;
