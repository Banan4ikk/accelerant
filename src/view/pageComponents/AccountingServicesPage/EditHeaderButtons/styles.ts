import styled from 'styled-components';

type Props = {
  background: string;
  border?: string;
  color: string;
};

export const ButtonsContainer = styled.div`
  display: flex;
`;

export const StyledImg = styled.img`
  margin-right: 5px;
`;

export const StyledButton = styled.div<Props>`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 6px;
  padding: 10px 20px;
  color: ${props => props.color};
  border: ${props => (props.border ? props.border : 'none')};
  background: ${props => props.background};
  margin-right: 10px;
`;
