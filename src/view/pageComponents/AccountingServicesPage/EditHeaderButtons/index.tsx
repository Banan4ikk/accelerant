import React from 'react';
import {ButtonsContainer, StyledButton, StyledImg} from './styles';

const EditHeaderButtons = () => {
  return (
    <ButtonsContainer>
      <StyledButton
        background="#fff"
        color="#122434"
        border="1px solid #CAF0FF;">
        <StyledImg src="/images/edit.svg" />
        Edit
      </StyledButton>
      <StyledButton background="#FFFAFA" color="#F05658">
        Delete
      </StyledButton>
    </ButtonsContainer>
  );
};

export default EditHeaderButtons;
